size_t
crypto_kdf_bytes_max(void);

size_t
crypto_kdf_contextbytes(void);

size_t
crypto_kdf_keybytes(void);

int
crypto_kdf_derive_from_key(unsigned char *subkey, size_t subkey_len,
                           uint64_t subkey_id,
                           const char ctx[],
                           const unsigned char key[]);