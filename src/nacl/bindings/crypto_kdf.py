
from nacl import exceptions as exc
from nacl._sodium import ffi, lib
from nacl.exceptions import ensure

crypto_kdf_CONTEXTBYTES = lib.crypto_kdf_contextbytes()
crypto_kdf_KEYBYTES = lib.crypto_kdf_keybytes()

def crypto_kdf(secret, context, keyid, keylen=32):
    """
    Derives a key for the specified `context` from `secret`.

    :param secret: bytes
    :param context: bytes of length `crypto_kdf_CONTEXTBYTES`.
    :rtype: bytes of length `keylen`.
    """
    # crypto_kdf_derive_from_key(unsigned char *subkey, size_t subkey_len,
    #                        uint64_t subkey_id,
    #                        const char ctx[crypto_kdf_CONTEXTBYTES],
    #                        const unsigned char key[crypto_kdf_KEYBYTES])
    result = ffi.new("unsigned char[]", keylen)
    ctx = ffi.new("char[]", context)
    rc = lib.crypto_kdf_derive_from_key(result, keylen, keyid, ctx, secret)
    ensure(rc == 0, "Unexpected library error", raising=exc.RuntimeError)
    return ffi.buffer(result, keylen)[:]